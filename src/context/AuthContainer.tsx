import { SpinStretch } from "react-cssfx-loading";
import { useKindeAuth } from "@kinde-oss/kinde-auth-react";
import RootPage from "../pages/Root";
import { GoogleGeminiEffectDemo } from "../pages/GoogleGeminiEffectDemo";

export default function AuthContainer() {
  const { isAuthenticated, isLoading, login } = useKindeAuth();

  if(isLoading){
    return (
      <div className="w-screen h-screen flex justify-center items-center">
        <SpinStretch />
      </div>
    )
  }
  return (
    <div>
      {isAuthenticated ? (
        <RootPage />
      ) : (
        <div>
          <GoogleGeminiEffectDemo onClick={() => login()} />
        </div>
      )}
    </div>
  );
}
