import React from 'react'
import { Route, Routes } from 'react-router-dom'
import Home from '../pages/Home'
import Album from '../pages/Album'
import Playlist from '../pages/Playlist'
import Category from '../pages/Category'
import Artist from '../pages/Artist'
import Search from '../pages/Search'
import TrendingPage from '../pages/TrendingPage'
import NewReleaseTrackPage from '../pages/NewReleaseTrackPage'
import TopPlayListPage from '../pages/TopPlayListPage'
import FeaturedPlayListPage from '../pages/FeaturedPlayList'
import CategoriesPage from '../pages/CategoriesPage'

export default function RouterContainer() {
  return (
    <div className="min-h-[calc(100vh-144px)]">
        <Routes>
          <Route index element={<Home />} />
          <Route path='trending' element={<TrendingPage />} />
          <Route path='new-releases' element={<NewReleaseTrackPage />} />
          <Route path='top-playlists' element={<TopPlayListPage />} />
          <Route path='featured-playlists' element={<FeaturedPlayListPage />} />
          <Route path='categories' element={<CategoriesPage />} />
          <Route path="album/:id" element={<Album />} />
          <Route path="playlist/:id" element={<Playlist />} />
          <Route path="category/:id" element={<Category />} />
          <Route path="artist/:id" element={<Artist />} />
          <Route path="search" element={<Search />} />
        </Routes>
      </div>
  )
}
