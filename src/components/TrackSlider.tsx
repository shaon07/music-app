import React from 'react'
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Pagination } from "swiper/modules";
import { FaPlay } from "react-icons/fa";

type TrackSliderProps = {
    data:any;
    handler:Function
}

export default function TrackSlider({data, handler}: TrackSliderProps) {
  return (
    <Swiper
        slidesPerView={2}
        spaceBetween={30}
        pagination={{
          clickable: true,
        }}
        modules={[Pagination,Autoplay]}
        className="mySwiper"
        autoplay={{
            delay: 2500,
            disableOnInteraction: false,
          }}
        breakpoints={{
            640: {
              slidesPerView: 2,
              spaceBetween: 20,
            },
            768: {
              slidesPerView: 4,
              spaceBetween: 40,
            },
            1024: {
              slidesPerView: 6,
              spaceBetween: 50,
            },
          }}
      >
        {data.map((item:any) => (
          <SwiperSlide key={item.id}>
            <div key={item.id}>
              <div
                onClick={() => {
                    handler(item.id);
                }}
                className="w-full transition duration-300 bg-dark hover:bg-dark-hovered p-2 rounded-md relative group cursor-pointer"
              >
                <div className="w-full h-0 pb-[100%] relative bg-gray-800">
                  <img
                    className="absolute w-full h-full object-cover rounded-md group-hover:brightness-[80%] transition duration-300"
                    src={item.image}
                    alt=""
                  />
                  <div className="h-10 w-10 border rounded-full absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 flex justify-center items-center opacity-0 group-hover:opacity-100 transition duration-300">
                    <FaPlay className="fill-white w-5 h-5" />
                  </div>
                </div>
                <p className="mt-2 font-medium line-clamp-2">{item.title}</p>
                {!!item.description && (
                  <p className="text-gray-400 line-clamp-2">
                    {item.description}
                  </p>
                )}
              </div>
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
  )
}
