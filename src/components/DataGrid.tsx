import { FC } from "react";
import { FaPlay } from "react-icons/fa";
import { Link } from "react-router-dom";
import MusicCard from "./MusicCard";

export interface MusicCardProps {
  data: {
    id: string;
    image: string;
    title: string;
    description?: string;
  }[];
  type: "link" | "button";
  handler: Function;
}

const DataGrid: FC<MusicCardProps> = ({ data, type, handler }) => {
  return (
    <div className="grid grid-cols-[repeat(auto-fill,_minmax(150px,_1fr))] md:grid-cols-[repeat(auto-fill,_minmax(180px,_1fr))] gap-3">
      <MusicCard data={data} type={type} handler={handler} />
    </div>
  );
};

export default DataGrid;
