export const getAccountInfo = async () => {
    return await fetch("https://accounts.spotify.com/api/token", {
        method: "POST",
        headers: {
          Authorization: `Basic ${btoa(
            `${import.meta.env.VITE_CLIENT_ID}:${
              import.meta.env.VITE_CLIENT_SECRET
            }`
          )}`,
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: "grant_type=client_credentials",
      })
}