import Error from "../components/Error";
import { FC } from "react";
import Loader from "../components/Loader";
import { PlayerContext } from "../context/PlayerContext";
import { getHomeContent } from "../services/home";
import { useContext } from "react";
import useSWR from "swr";
import TrackSlider from "../components/TrackSlider";
import { Link, useNavigate } from "react-router-dom";

const Home: FC = () => {
  let navigate = useNavigate();
  const { setPlayerId, setIsPlayerIdChanged } = useContext(PlayerContext);

  const { data, error } = useSWR("home", () => getHomeContent(), {
    revalidateOnFocus: false,
    revalidateIfStale: false,
  });

  if (error) return <Error />;

  if (!data) return <Loader />;

  const trendingTracks = data.recommendations.tracks
    .filter((track) => track.name)
    .map((track) => ({
      id: track.id,
      image: (track as any)?.album?.images?.[0]?.url,
      title: track.name,
      description: track?.artists.map((artist) => artist.name).join(", "),
    }));

  const newReleasesTracks = data.newReleases.albums.items
    .filter((album) => album.name)
    .map((album) => ({
      id: album.id,
      image: album.images?.[0]?.url,
      title: album.name,
      description: (album as any)?.artists
        ?.map((artist: any) => artist?.name)
        ?.join(", "),
    }));

  const topPlayList = data.topPlaylists
    .filter((playlist) => playlist.name)
    .map((playlist) => ({
      id: playlist.id,
      image: playlist.images?.[0]?.url,
      title: playlist.name,
      description: playlist?.owner?.display_name,
    }));

  const featuredPlaylists = data.featuredPlaylists.playlists.items
    .filter((playlist) => playlist.name)
    .map((playlist) => ({
      id: playlist.id,
      image: playlist.images?.[0]?.url,
      title: playlist.name,
      description: playlist?.owner?.display_name,
    }));

  const categories = data.categories.categories.items.map((category) => ({
    id: category.id,
    image: category.icons?.[0]?.url,
    title: category.name,
  }));

  return (
    <div className="mx-[5vw] pb-6">
      <div className="flex items-center justify-between">
        <h1 className="mt-5 mb-3 text-2xl">Trending Now</h1>
        <Link to="/trending" className="text-gray-400 underline">
          See All
        </Link>
      </div>
      <TrackSlider
        data={trendingTracks}
        handler={(id: string) => {
          setPlayerId(id);
          setIsPlayerIdChanged(true);
        }}
      />

      <div className="flex items-center justify-between">
        <h1 className="mt-5 mb-3 text-2xl">New Releases</h1>
        <Link to="/new-releases" className="text-gray-400 underline">
          See All
        </Link>
      </div>
      <TrackSlider
        data={newReleasesTracks}
        handler={(id: string) => {
          navigate(`/album/${id}`);
        }}
      />

      <div className="flex items-center justify-between">
        <h1 className="mt-5 mb-3 text-2xl">Top Playlists</h1>
        <Link to="/top-playlists" className="text-gray-400 underline">
          See All
        </Link>
      </div>
      <TrackSlider
        handler={(id: string) => navigate(`/playlist/${id}`)}
        data={topPlayList}
      />

      <div className="flex items-center justify-between">
        <h1 className="mt-5 mb-3 text-2xl">Featured Playlists</h1>
        <Link to="/featured-playlists" className="text-gray-400 underline">
          See All
        </Link>
      </div>
      <TrackSlider
        handler={(id: string) => navigate(`/playlist/${id}`)}
        data={featuredPlaylists}
      />

      <div className="flex items-center justify-between">
        <h1 className="mt-5 mb-3 text-2xl">Categories</h1>
        <Link to="/categories" className="text-gray-400 underline">
          See All
        </Link>
      </div>
      <TrackSlider
        handler={(id: string) => navigate(`/category/${id}`)}
        data={categories}
      />
    </div>
  );
};

export default Home;
