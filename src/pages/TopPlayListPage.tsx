import DataGrid from "../components/DataGrid";
import { getHomeContent } from "../services/home";
import useSWR from "swr";
import Error from "../components/Error";
import { Loader } from "@react-three/drei";

export default function TopPlayListPage() {

  const { data, error } = useSWR("home", () => getHomeContent(), {
    revalidateOnFocus: false,
    revalidateIfStale: false,
  });

  if (error) return <Error />;

  if (!data) return <Loader />;

  const topPlayList = data.topPlaylists
    .filter((playlist) => playlist.name)
    .map((playlist) => ({
      id: playlist.id,
      image: playlist.images?.[0]?.url,
      title: playlist.name,
      description: playlist?.owner?.display_name,
    }));

  return (
    <div className="mx-[5vw] pb-6">
      <h1 className="mt-5 mb-3 text-2xl">Top Playlists</h1>

      <DataGrid
        type="link"
        handler={(id: string) => `/playlist/${id}`}
        data={topPlayList}
      />
    </div>
  );
}
