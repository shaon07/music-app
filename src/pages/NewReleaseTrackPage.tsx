import DataGrid from "../components/DataGrid";
import { getHomeContent } from "../services/home";
import useSWR from "swr";
import Error from "../components/Error";
import { Loader } from "@react-three/drei";

export default function NewReleaseTrackPage() {

  const { data, error } = useSWR("home", () => getHomeContent(), {
    revalidateOnFocus: false,
    revalidateIfStale: false,
  });

  if (error) return <Error />;

  if (!data) return <Loader />;

  const newReleasesTracks = data.newReleases.albums.items
    .filter((album) => album.name)
    .map((album) => ({
      id: album.id,
      image: album.images?.[0]?.url,
      title: album.name,
      description: (album as any)?.artists
        ?.map((artist: any) => artist?.name)
        ?.join(", "),
    }));

  return (
    <div className="mx-[5vw] pb-6">
      <h1 className="mt-5 mb-3 text-2xl">New Releases</h1>

      <DataGrid
        type="link"
        handler={(id: string) => `/album/${id}`}
        data={newReleasesTracks}
      />
    </div>
  );
}
