import DataGrid from "../components/DataGrid";
import { getHomeContent } from "../services/home";
import useSWR from "swr";
import Error from "../components/Error";
import { Loader } from "@react-three/drei";

export default function FeaturedPlayListPage() {
  const { data, error } = useSWR("home", () => getHomeContent(), {
    revalidateOnFocus: false,
    revalidateIfStale: false,
  });

  if (error) return <Error />;

  if (!data) return <Loader />;

  const featuredPlaylists = data.featuredPlaylists.playlists.items
    .filter((playlist) => playlist.name)
    .map((playlist) => ({
      id: playlist.id,
      image: playlist.images?.[0]?.url,
      title: playlist.name,
      description: playlist?.owner?.display_name,
    }));

  return (
    <div className="mx-[5vw] pb-6">
      <h1 className="mt-10 mb-3 text-2xl">Featured Playlists</h1>

      <DataGrid
        type="link"
        handler={(id: string) => `/playlist/${id}`}
        data={featuredPlaylists}
      />
    </div>
  );
}
