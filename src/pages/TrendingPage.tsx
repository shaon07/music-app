import { useContext } from "react";
import { PlayerContext } from "../context/PlayerContext";
import { getHomeContent } from "../services/home";
import useSWR from "swr";
import Error from "../components/Error";
import Loader from "../components/Loader";
import DataGrid from "../components/DataGrid";

export default function TrendingPage() {
  const { setPlayerId, setIsPlayerIdChanged } = useContext(PlayerContext);

  const { data, error } = useSWR("home", () => getHomeContent(), {
    revalidateOnFocus: false,
    revalidateIfStale: false,
  });

  if (error) return <Error />;

  if (!data) return <Loader />;

  const trendingTracks = data.recommendations.tracks
    .filter((track) => track.name)
    .map((track) => ({
      id: track.id,
      image: (track as any)?.album?.images?.[0]?.url,
      title: track.name,
      description: track?.artists.map((artist) => artist.name).join(", "),
    }));

  return (
    <div className="mx-[5vw] pb-6">
      <h1 className="mt-5 mb-3 text-2xl">Trending Now</h1>
      <DataGrid
        type="button"
        handler={(id: string) => {
          setPlayerId(id);
          setIsPlayerIdChanged(true);
        }}
        data={trendingTracks}
      />
    </div>
  );
}
