import DataGrid from "../components/DataGrid";
import { getHomeContent } from "../services/home";
import useSWR from "swr";
import Error from "../components/Error";
import { Loader } from "@react-three/drei";

export default function CategoriesPage() {
  const { data, error } = useSWR("home", () => getHomeContent(), {
    revalidateOnFocus: false,
    revalidateIfStale: false,
  });

  if (error) return <Error />;

  if (!data) return <Loader />;

  const categories = data.categories.categories.items.map((category) => ({
    id: category.id,
    image: category.icons?.[0]?.url,
    title: category.name,
  }));


  return (
    <div className="mx-[5vw] pb-6">
      <h1 className="mt-10 mb-3 text-2xl">Categories</h1>

      <DataGrid
        type="link"
        handler={(id: string) => `/category/${id}`}
        data={categories}
      />
    </div>
  );
}
