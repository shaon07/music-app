import { useLocation } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import { PlayerContext } from "../context/PlayerContext";
import client from "../shared/spotify-client";
import { SpinStretch } from "react-cssfx-loading";
import Navbar from "../components/NavBar";
import RouterContainer from "../context/RouterContainer";
import Player from "../components/Player";
import { getAccountInfo } from "../services/accounts";


enum LoadingStates {
  loading,
  finished,
  error,
}

export default function RootPage() {
  const [loadingState, setLoadingState] = useState(LoadingStates.loading);

  const { playerId } = useContext(PlayerContext);

  useEffect(() => {
    localStorage.setItem("shaon-playing", playerId);
  }, [playerId]);

  const location = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location]);

  useEffect(() => {
    getAccountInfo()
      .then((res) => res.json())
      .then((data) => {
        if (data.access_token) {
          setLoadingState(LoadingStates.finished);
          client.setAccessToken(data.access_token);
        } else setLoadingState(LoadingStates.error);
      })
      .catch((err) => {
        console.log(err);
        setLoadingState(LoadingStates.error);
      });
  }, []);

  if (loadingState === LoadingStates.loading)
    return (
      <div className="w-screen h-screen flex justify-center items-center">
        <SpinStretch />
      </div>
    );

  if (loadingState === LoadingStates.error)
    return <div>Something went wrong</div>;

  return (
    <>
      <Navbar />
      <RouterContainer />
      {!!playerId && <Player key={playerId} />}
    </>
  );
}
