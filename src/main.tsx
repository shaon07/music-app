import "./index.css";
// Import Swiper styles
import 'swiper/css';
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { PlayerContextProvider } from "./context/PlayerContext";
import ReactDOM from "react-dom/client";
import { KindeProvider } from "@kinde-oss/kinde-auth-react";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <KindeProvider
    clientId={import.meta.env.VITE_CLIENT_KINDE_ID}
    domain={import.meta.env.VITE_CLIENT_KINDE_URL}
    redirectUri={import.meta.env.VITE_CLIENT_mode === "production" ? "https://music-app-phi-one.vercel.app" : "http://localhost:3000"}
    logoutUri={import.meta.env.VITE_CLIENT_mode === "production" ? "https://music-app-phi-one.vercel.app" : "http://localhost:3000"}
  >
    <PlayerContextProvider>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </PlayerContextProvider>
  </KindeProvider>
);
