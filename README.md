# shaon

A music website built with Sportify API


## Live demo

Official Website: [https://music-app-phi-one.vercel.app](https://music-app-phi-one.vercel.app)

## Main technology used

- react, typescript, tailwind

## Features

- Home
  - Get new releases
  - Featured Playlists
  - Recommendation
  - Categories
  - Top Playlists
- Search
- Artist
- Music Player
- Open in Spotify

## Installation

- Clone the project
- Run `npm install`
- Create your own spotify developer project: [https://developer.spotify.com/dashboard](https://developer.spotify.com/dashboard)
- Example .env file:

```env
VITE_CLIENT_ID=7e1578ceb6524e4ea6b385a60d2933cf
VITE_CLIENT_SECRET=c3014bd3d1934b2cb77427245a8728d0
VITE_CLIENT_KINDE_ID=928bc029454f4c888fb126a8c8edcaff
VITE_CLIENT_KINDE_URL=https://shaon.kinde.com
VITE_CLIENT_mode=development
```

